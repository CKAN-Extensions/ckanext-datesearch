# "Filter by Time" extension for CKAN

This ckan plugin has been cloned from https://github.com/EUDAT-B2FIND/ckanext-datesearch and updated 
by **Francesco Mangiacrapa** according to the needs of D4science and its communities

## Authors

* **Francesco Mangiacrapa** ([ORCID](https://orcid.org/0000-0002-6528-664X)) Computer Scientist at [ISTI-CNR Infrascience Group](http://nemis.isti.cnr.it/groups/infrascience) 

## License

This project is licensed under the EUPL V.1.1 License - see the [LICENSE.md](LICENSE.md) file for details.

## Contains external libraries
- [bootstrap-datepicker.js](https://github.com/eternicode/bootstrap-datepicker/), Apache License 2.0
- [Moment.js](http://momentjs.com/), MIT License

### Originally sources
The "Filter by year", see at [UDAT-B2FIND](https://github.com/EUDAT-B2FIND/ckanext-datesearch)
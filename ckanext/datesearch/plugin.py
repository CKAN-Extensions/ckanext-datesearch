import logging

import ckan.plugins as plugins
import ckan.plugins.toolkit as toolkit

log = logging.getLogger(__name__)


class DateSearchPlugin(plugins.SingletonPlugin):
    plugins.implements(plugins.IConfigurer)
    plugins.implements(plugins.IPackageController, inherit=True)

    def update_config(self, config):
        toolkit.add_template_directory(config, 'templates')
        toolkit.add_resource('fanstatic', 'ckanext-datesearch')

    def before_search(self, search_params):
        extras = search_params.get('extras')
        if not extras:
            # There are no extras in the search params, so do nothing.
            return search_params

        start_date = extras.get('ext_startdate')

        end_date = extras.get('ext_enddate')

        if not start_date and not end_date:
            # The user didn't select either a start and/or end date, so do nothing.
            return search_params
        if not start_date:
            start_date = '*'
        if not end_date:
            end_date = '*'

        # Add a date-range query with the selected start and/or end dates into the Solr facet queries.
        #fq = search_params.get('fq', '')
        #fq = '{fq} +extras_PublicationTimestamp:[{sd} TO {ed}]'.format(fq=fq, sd=start_date, ed=end_date)
        #fq = '{fq} +metadata_modified:[{start_date} TO {end_date}]'.format(fq=fq, start_date=start_date, end_date=end_date)
        #search_params['fq'] = fq
        
        # Updated by Francesco see: https://github.com/geosolutions-it/ckanext-datesearch
        # Add a date-range query with the selected start and/or end dates into the Solr queries.
        #MAYBE CAN BE OPTIMIZED
        fq = search_params.get('fq', '')
        fq = '({fq} -time_date:[* TO *])'.format(fq=fq) + ' OR ({fq} +time_date:[{start_date} TO {end_date}])'.format(fq=fq, start_date=start_date, end_date=end_date)
        
        #NB. I'M CHANGING THE QUERY NOT THE FACET QUERY
        search_params['q'] = fq
        log.debug('search_params["q"] is: '+str(search_params['q']))

        return search_params
